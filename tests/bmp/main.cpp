#include <iostream>
#include <sstream>

#include "BMP.h"
#include "Mnist.h"
#include "Vector.h"
#include "Utils.h"

int main(int argc, char **argv) {
  BMP inp1{"in/9_1.bmp"};
  {
    std::ofstream of("9_1.csv");
    of << 9 << "," << inp1 << std::endl;
  }

  utils::for_file_line("9_1.csv", [](size_t i,
                                     size_t bound,
                                     const std::string &line) {
    std::stringstream ss(line);
    std::string s;
    size_t symbol = 0;
    std::vector<uint8_t> request(784);
    int digit = -1;
    while (std::getline(ss, s, ',')) {
      if (!symbol) {
        digit = std::stoi(s);
      } else {
        request[symbol - 1] = std::stoi(s);
      }
      ++symbol;
    }
    BMP out(28, 28, false);
    mnist::Utils::vector2bmp(request, out);
    std::stringstream fns;
    fns << "9_1_rewrite.bmp";
    out.write(fns.str().c_str());
  }, 2'000);

  return 0;
}