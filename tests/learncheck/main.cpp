#include <iostream>

#include "Mnist.h"

using namespace mnist;

constexpr const size_t input_neurons = 784;
constexpr const size_t median_neurons = 200;
constexpr const size_t output_neurons = 10;

int main(int argc, char **argv) {
  std::cout.imbue(std::locale(""));

  MnistNNTrainer mt{MnistNNCreator::spawn_nn()};
  mt.train_nn(60'000);
  mt.check_nn();

  return 0;
}
