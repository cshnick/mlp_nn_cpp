#include <iostream>
#include <random>

#include "FreeMath.h"
#include "Constants.h"
#include "Utils.h"

using namespace mlp;
using namespace utils;
int main(int argc, char **argv) {
  double x, y, z, a, b, c, d;
  constexpr const int exp_num = 10;

  constexpr const double lower(-0.5), upper(0.5);
  utils::print_measure([&]() { ; });
  std::cout << "========" << std::endl;
  utils::print_measure([&]() { x = rand_weight(); });
  utils::print_measure([&]() { y = rand_weight(); });
  utils::print_measure([&]() { z = rand_weight(); });
  utils::print_measure([&]() { a = rand_weight(); });
  utils::print_measure([&]() { b = rand_weight(); });
  utils::print_measure([&]() { c = rand_weight(); });
  utils::print_measure([&]() { d = rand_weight(); });

  std::cout << "x random :" << x << std::endl
            << "y random: " << y << std::endl
            << "z random: " << z << std::endl
            << "a random: " << a << std::endl
            << "b random: " << b << std::endl
            << "c random: " << c << std::endl
            << "d random: " << d << std::endl;

  return 0;
}
