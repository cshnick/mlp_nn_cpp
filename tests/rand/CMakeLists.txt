cmake_minimum_required(VERSION 2.8)

get_filename_component(current_dir ${CMAKE_CURRENT_SOURCE_DIR} NAME)
set(target_name "${current_dir}_test")
message("target name: ${target_name}")

set(CMAKE_CXX_FLAGS_RELEASE "-O2")
set(CMAKE_CXX_FLAGS_DEBUG  "-g -O0")

include_directories(${common_include})
add_definitions(-DCMAKE_CURRENT_FILENAME="${current_dir}")

set(headers)
set(sources main.cpp)

add_executable(${target_name} ${sources} ${headers})
target_link_libraries(${target_name} m)