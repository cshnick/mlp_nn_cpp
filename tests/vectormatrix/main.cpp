#include <iostream>
#include <iomanip>

#include "Constants.h"
#include "Utils.h"
#include "Matrix.h"
#include "Vector.h"

using namespace mlp;
using namespace utils;

void fill_vector(mlp::Vector &m) {
  for (int i = 0; i < m.width(); ++i) {
    m[i] = rand_weight();
  }
}
void print_matrix(Matrix &m) {
  std::cout << "Matrix of size {" << m.sig_count() << "," << m.out_count() << "}" << std::endl;
  std::cout << "[" << std::endl;
  for (int i = 0; i < m.sig_count(); ++i) {
    std::cout << "[";
    for (int j = 0; j < m.out_count(); ++j) {
      std::cout << m[i][j] << ", ";
    }
    std::cout << "]" << std::endl;
  }
  std::cout << "]" << std::endl;
}
void matrix_constructor_benchmark(Size msz, size_t count) {
  for (int i = 0; i < count; ++i) {
    std::cout << std::setfill(' ') << std::setw(3) << i + 1 << ". "
              << "Matrix v of size {" << msz.first << "," << msz.second << "} constructor    RANDOM: "
              << measure([=]() { Matrix m{msz, Matrix::RANDOM}; }) << " ns" << std::endl;
    std::cout << "     Matrix v of size {" << msz.first << "," << msz.second << "} constructor    ZEROES: "
              << measure([=]() { Matrix m{msz, Matrix::ZEROES}; }) << " ns" << std::endl;
    std::cout << "     Matrix v of size {" << msz.first << "," << msz.second << "} constructor  UNDEFINED: "
              << measure([=]() { Matrix m{msz, Matrix::UNDEFINED}; }) << " ns" << std::endl;
    std::cout << std::endl;
  }
}
void vector_constructor_benchmark(size_t width, size_t count) {
  for (int i = 0; i < count; ++i) {
    Vector v{width};
    std::cout << std::setfill(' ') << std::setw(3) << i + 1 << ". "
              << "Vector [v" << std::setfill(' ') << std::setw(3) << i + 1
              << "] of width " << width << " constructor: "
              << measure([=]() { Vector vc{width};; }) << " ns" << std::endl;
    std::cout << "     Vector [v" << std::setfill(' ') << std::setw(3) << i + 1
              << "] of width " << width << " fill_vector: "
              << measure([&]() { fill_vector(v); }) << " ns" << std::endl;
  }
}

void swap_vector_test() {
  std::cout << "Vector swap test\n=================" << std::endl;
  Vector v1{784}, v2{0};
  v2.swap(std::move(v1));
  std::cout << "Swap finished" << std::endl << std::endl;
}
void set_vector_test(Vector &v, size_t index, double value, const std::string &name) {
  double old = v[index];
  if (old == value) {
    throw std::runtime_error("Expected and and presenting values are the same");
  }
  std::string result;
  v[index] = value;
  if (v[index] == value) {
    result = "] === SUCCESS";
  } else {
    result = "] === FAILURE";
  }
  std::cout << "For vector " << name << " setting " << name
            << "[" << std::setfill(' ') << std::setw(3) << index
            << "]; old value " << old
            << ", expected value [" << value
            << "], set value [" << v[index] << result << std::endl;
}
void max_feed_test() {
  std::cout << "maxfeed vector test\n=================" << std::endl;
  Vector v{10};
  for (int i = 0; i < 10; ++i) {
    v[i] = freemath::rand(-0.5, 0.5);
  }
  int expected = 8, got = -1;
  v[expected] = 0.917;
  got = freemath::max_feed(v);
  std::cout << "maxfeed expected: " << expected
            << "; freemax::maxfeed: " << got << " - "
            << (got == expected ? "SUCCESS" : "FAILURE") << std::endl;
  std::cout << std::endl;
};
void set_matrix_test(Matrix &m, Size msz, double value, const std::string &name) {
  double old = m[msz.first][msz.second];
  if (old == value) {
    throw std::runtime_error("Expected and and presenting values are the same");
  }
  std::string result;
  m[msz.first][msz.second] = value;
  if (m[msz.first][msz.second] == value) {
    result = "] === SUCCESS";
  } else {
    result = "] === FAILURE";
  }
  std::cout << "For matrix " << name << " setting " << name
            << "[" << std::setfill(' ') << std::setw(3) << msz.first << "]"
            << "[" << std::setfill(' ') << std::setw(3) << msz.second << "]"
            << "; old value " << old
            << ", expected value [" << value
            << "], set value [" << m[msz.first][msz.second] << result << std::endl;
}

int main(int argc, char **argv) {
  std::cout << " === Test for " << CMAKE_CURRENT_FILENAME << " === " << std::endl << std::endl;

  Size sz{784, 200};

  std::cout << " === Vector operations === " << std::endl << std::endl;

  std::cout << "Vector constructor benchmark" << "\n=================" << std::endl;
  vector_constructor_benchmark(sz.first, 5);
  std::cout << std::endl;
  std::cout << "Set value behavior\n=================" << std::endl;
  Vector v{sz.first}, v1(sz.first), v2(sz.first);
  set_vector_test(v, 201, 600.0, "v1");
  set_vector_test(v1, 0, 467.0, "v2");
  set_vector_test(v2, 783, 821.0, "v3");
  std::cout << std::endl;

  swap_vector_test();
  max_feed_test();

  std::cout << " === Matrix operations" << " === " << std::endl << std::endl;

  std::cout << "Matrix constructor benchmark" << "\n=================" << std::endl;
  matrix_constructor_benchmark(sz, 5);
  std::cout << "Set value behavior\n=================" << std::endl;
  Matrix m1(sz), m2(sz), m3(sz);
  set_matrix_test(m1, {0, 0}, 135.0, "m1");
  set_matrix_test(m2, {450, 100}, 533.0, "m2");
  set_matrix_test(m3, {784, 199}, 784.0, "m3");
  std::cout << std::endl;

  std::cout << "Printing some data to the screen, press Enter..." << std::endl;
  std::cin.get();
  std::cout << "=================" << std::endl;

  Matrix m11({10, 2});
  m11.print();

  std::cout << "Double: " << m11[9][1] << std::endl;
  std::cout << "Double: " << m11[0][0] << std::endl;
//  print_matrix(m1);
//  v.print();
//  v1.print();
//  v2.print();

  return 0;
}
