#include <iostream>

#include <cmath>

#include "FreeMath.h"
#include "Utils.h"

int main(int argc, char **argv) {
  double x, y;
  constexpr const int exp_num = 10;
  utils::print_measure([&]() { 100 * 400; });
  std::cout << "========" << std::endl;
  utils::print_measure([&]() { ; });
  utils::print_measure([&]() { x = freemath::exp::calc(exp_num); });
  utils::print_measure([&]() { y = exp(exp_num); });

  std::cout << "exp(" << 4 << ")" << std::endl
            << "exp::evaluate: " << x << std::endl
            << "exp math.h: " << y << std::endl;

  return 0;
}
