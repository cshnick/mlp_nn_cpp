#include <iostream>
#include <vector>
#include <sstream>

#include "Mnist.h"
#include "BMP.h"
#include "Utils.h"

int main(int argc, char **argv) {
  utils::for_file_line("mnist_test.csv", [](size_t i,
                                            size_t bound,
                                            const std::string &line) {
    if (!i) return; // Skip first line;
    std::stringstream ss(line);
    std::string s;
    size_t symbol = 0;
    std::vector<uint8_t> request(784);
    int digit = -1;
    while (std::getline(ss, s, ',')) {
      if (!symbol) {
        digit = std::stoi(s);
      } else {
        request[symbol - 1] = std::stoi(s);
      }
      ++symbol;
    }
    BMP out(28, 28, false);
    mnist::Utils::vector2bmp(request, out);
    std::stringstream fns;
    fns << "out/" << digit << "_" << i << ".bmp";
    out.write(fns.str().c_str());
  }, 1'000);

  std::cout << "Ok" << std::endl;

  return 0;
}
