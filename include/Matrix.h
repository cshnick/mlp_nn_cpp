#pragma once

#include <functional>
#include <memory>

#include "Constants.h"

namespace mlp {

/**
 *  Despite interacting as a general Matrix, i.e. vector<vector<double>>
 *  or so, mlp::Matrix has a hidden layer, which stores additional
 *  vector. That means if matrix has a size of {100, 100} - there is
 *  a 100th hidden layer. Access to \b matrix[100][99] is \b acceptable.
 */

class Matrix {
  using DataDeleterType = std::function<void(double *[])>;

public:
  enum InitOpt {
    RANDOM,
    ZEROES,
    UNDEFINED
  };

  explicit Matrix(Size sz, InitOpt initwith = RANDOM);
  double *&operator[](size_t index);

  Size size();
  [[nodiscard]] size_t sig_count() const;
  [[nodiscard]] size_t out_count() const;
  void swap(Matrix &&other);
  void print() const;

public:
  ~Matrix() = default;
  Matrix(const Matrix &other) = delete;
  Matrix &operator=(const Matrix &other) = delete;
  Matrix() = delete;

private:
  void init_random();
  void init_zeroes();

private:
  Size size_;
  std::unique_ptr<double *[], DataDeleterType> data;
};

} // namespace mlp