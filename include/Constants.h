#pragma once

#include <FreeMath.h>

namespace mlp {

using Size = std::pair<size_t, size_t>;
using MLPNNStructure = std::vector<size_t>;

constexpr static const double lr = 0.5;

inline double rand_weight() {
  return freemath::rand(-0.5, 0.5);
}

} // namespace mlp
