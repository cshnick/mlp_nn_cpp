#pragma once

#include "Constants.h"
#include "MLPNNLayer.h"

namespace mlp {
class MLPNeuralNetwork {
  using Layers = std::vector<MLPNNLayer>;

public:
  explicit MLPNeuralNetwork(MLPNNStructure &&structure);
  /**
   *    Get through all layers in forward direction,
   *  recalculating weights, necessary when training
   *  neurons.
   *    Also used to feed result vector when
   *  retrieving NN reply
   */
  void feed_forward();
  void back_propagate();
  void new_lesson(Vector &&request, Vector &&result);
  void feed(Vector &&request);
  size_t input_neurons_size();
  size_t output_neurons_size();
  Vector &result_vector();

private:
  Layers layers_;
  std::vector<size_t> structure_;
  Vector request_;
  Vector expected_;
};

} // namespace mlp;