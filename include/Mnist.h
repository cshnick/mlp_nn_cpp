#pragma once

#include <iostream>
#include <vector>

#include "Constants.h"

namespace mlp {
class Vector;
} //namespace mlp
struct BMP;

std::ostream &operator<<(std::ostream &out, const mlp::Vector &v);
std::ostream &operator<<(std::ostream &out, const BMP &bmp);

namespace mlp {
class MLPNeuralNetwork;
class Vector;
}

namespace mnist {
using NNSPtr = std::shared_ptr<mlp::MLPNeuralNetwork>;

class MnistNNCreator {
public:
  static NNSPtr spawn_nn();
};

class MnistNNTrainer {
public:
  explicit MnistNNTrainer(NNSPtr nn) noexcept;

  void train_nn(size_t iterations = SIZE_MAX);
  void check_nn();
  void feed(mlp::Vector &v);
  size_t max_feed() const;
  [[nodiscard]] const mlp::Vector &result_vector() const;
  [[nodiscard]]int result() const;
private:
  NNSPtr nn_;
};

class Utils {
public:
  static void vector2bmp(const std::vector<uint8_t> &v, BMP &m);
};

} // namespace mnist