#pragma once

#include <functional>
#include <memory>

namespace mlp {

class Vector {
  using DataDeleterType = std::function<void(double [])>;

public:
  enum InitOpt {
    ZEROES,
    TINY_DOUBLE
  };
  explicit Vector(size_t width, InitOpt initwith = ZEROES);
  double &operator[](size_t index);
  const double &operator[](size_t index) const;
  [[nodiscard]] size_t width() const;
  void swap(Vector &&other);
  void print() const;

public:
  ~Vector() = default;
  Vector(const Vector &other) = delete;
  Vector &operator=(const Vector &other) = delete;
  Vector() = delete;

private:
  size_t width_;
  std::unique_ptr<double[], DataDeleterType> data;
};

} // namespace mlp

namespace freemath {
size_t max_feed(mlp::Vector &v);
}