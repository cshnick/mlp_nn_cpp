#pragma once

#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>

namespace utils {

using clock = std::chrono::high_resolution_clock;

void sleep(int seconds) {
  std::this_thread::sleep_for(std::chrono::seconds(seconds));
}

unsigned long duration(const std::chrono::time_point<std::chrono::system_clock> &start,
                       const std::chrono::time_point<std::chrono::system_clock> &finish) {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
}

template<typename Func, typename ...Args>
unsigned long measure(const Func &f, const Args &...args) {
  auto start = clock::now();
  f(args...);
  auto finish = clock::now();
  return duration(start, finish);
}

template<typename Func, typename ...Args>
void print_measure(const Func &f, const Args &...args) {
  std::cout << "Duration func: " << measure<Func, Args...>(f, args...) << " ns" << std::endl;
}

template<typename Func, typename  ...Args>
void for_file_line(const std::string &file, const Func &f, size_t bound = SIZE_MAX) {
  std::ifstream is(file);
  if (!is.is_open()) {
    throw std::runtime_error("File " + file + " is not opened");
  }
  std::string next_line;
  size_t iteration = 0;
  while (std::getline(is, next_line) and iteration < bound) {
    f(iteration++, bound, next_line);
  }
}

} // namespace utils