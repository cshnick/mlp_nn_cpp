#pragma once

#include <string>
#include <random>
#include <cmath>
#include <memory>

#include "Vector.h"
#include "Matrix.h"

namespace mlp {

class MLPNNLayer {
public:
  MLPNNLayer(size_t in, size_t out);
  MLPNNLayer(MLPNNLayer &&other) noexcept;

public:
  ~MLPNNLayer() = default;
  MLPNNLayer(const MLPNNLayer &other) = delete;
  MLPNNLayer &operator=(const MLPNNLayer &other) = delete;
  MLPNNLayer() = delete;

public:
  /**
   * Update weight matrix with calculated learn errors
   * @param data : input vector of signals size
   */
  void update_wm(Vector &data);
  /**
   * Skip current layer after signal
   * @param input
   */
  void feed_hidden(Vector &input);
  /**
   * Fill \b err_ vector from \b last layer
   * @param target
   */
  void feed_err_out(Vector &target);
  /**
   * Calculate \b err_ vector from from the
   * next layer if \b current is hidden
   * @param next
   */
  void feed_err_hid(MLPNNLayer &next);

  /**
   * Access to private members
   */
  Vector &hid_vector();

private:
  Matrix wm_;
  Vector hid_;
  Vector err_;
  Size lsz_;
};
} // namespace mlp