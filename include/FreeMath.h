#pragma once

#include <random>
#include <memory>
#include <array>
#include <functional>

namespace freemath {

template<typename Real, size_t degree, size_t i = 0>
struct Recursion {
  static Real calc(Real x) {
    constexpr Real c = 1.0 / static_cast<Real>(1u << degree);
    x = Recursion<Real, degree, i + 1>::calc(x);
    return x * x;
  }
};

template<typename Real, size_t degree>
struct Recursion<Real, degree, degree> {
  static Real calc(Real x) {
    constexpr Real c = 1.0 / static_cast<Real>(1u << degree);
    x = 1.0 + c * x;
    return x;
  }
};

inline double rand(double lower, double upper) {
  static std::random_device rd;
  static std::mt19937 mt(rd());
  std::uniform_real_distribution<double> dist(lower, upper);
  return dist(mt);
}

using exp = Recursion<double, 31>;

inline double sigmoid(double val, double a = 1) {
  return (1.0 / (1.0 + exp::calc(-a * val)));
}
inline double dsigmoid(double val) {
  return (val * (1.0 - val));
};

} // namespace FreeMath