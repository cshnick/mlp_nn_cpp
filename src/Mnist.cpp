#include "Mnist.h"

#include <iostream>
#include <iomanip>

#include "Utils.h"
#include "Vector.h"
#include "MLPNeuralNetwork.h"
#include "BMP.h"

using namespace mlp;

constexpr const size_t input_neurons = 784;
constexpr const size_t median_neurons = 200;
constexpr const size_t output_neurons = 10;

constexpr const size_t map_width = 28;
constexpr const size_t map_height = 28;

namespace mnist {

NNSPtr MnistNNCreator::spawn_nn() {
  return std::make_shared<MLPNeuralNetwork>(
      mlp::MLPNNStructure({input_neurons, median_neurons, output_neurons}));
}

MnistNNTrainer::MnistNNTrainer(NNSPtr nn) noexcept
    : nn_(std::move(nn)) {}

void MnistNNTrainer::train_nn(size_t iterations) {
  std::cout << "Training NN\n=========================" << std::endl;
  auto start = utils::clock::now();
  utils::for_file_line("mnist_train.csv", [this](size_t i,
                                                 size_t bound,
                                                 const std::string &line) {
    if (!i) return; // Skip first line;
    std::stringstream ss(line);
    std::string s;
    size_t symbol = 0;
    Vector request{input_neurons, Vector::TINY_DOUBLE};
    Vector expected{output_neurons, Vector::TINY_DOUBLE};
    while (std::getline(ss, s, ',')) {
      if (!symbol) {
        //Requested digit is equal to result vector index
        expected[std::stoi(s)] = 0.99;
      } else {
        // Gradient from 0.01 to 1.00
        double val = ((double) std::stoi(s) / 255 * 0.99) + 0.01;
        request[symbol - 1] = val;
      }
      ++symbol;
    }
    nn_->new_lesson(std::move(request), std::move(expected));
    std::cout << '\r' << i << " = " << i * bound / bound * 100 / bound << "%" << std::flush;
  }, iterations);
  auto finish = utils::clock::now();

  std::cout
      << std::endl
      << "=========================\nLearn duration: "
      << utils::duration(start, finish)
      << " ns" << std::endl;
}

void MnistNNTrainer::check_nn() {
  std::cout << "Checking resutls: " << std::endl;
  int cnt = 0, err = 0;
  auto start = utils::clock::now();
  utils::for_file_line("mnist_test.csv", [this, &cnt, &err](size_t i,
                                                            size_t bound,
                                                            const std::string &line) {
    if (!i) return; // Skip first line;
    std::stringstream ss(line);
    std::string s;
    size_t symbol = 0;
    int expected = 0;
    Vector request{input_neurons, Vector::TINY_DOUBLE};
    while (std::getline(ss, s, ',')) {
      if (!symbol) {
        expected = std::stoi(s);
      } else {
        // Gradient from 0.01 to 1.00
        double val = ((double) std::stoi(s) / 255 * 0.99) + 0.01;
        request[symbol - 1] = val;
      }
      ++symbol;
    }
    nn_->feed(std::move(request));

    int result = freemath::max_feed(nn_->result_vector());
    bool success = (result == expected);
    ++cnt;
    if (!success) ++err;
    std::cout << '\r' << i << " = "
              << std::setfill(' ') << std::setw(6)
              << (double) 100 - ((double) err * 100 / bound)
              << "% SUCCEDED" << std::flush;
  }, 10'000);
  auto finish = utils::clock::now();
  std::cout << std::endl;
  std::cout << "=========================\nSummary: "
            << err << " mismatches of " << cnt
            << "(" << (double) err * 100 / (cnt + 1) << "%)"
            << std::endl;
  std::cout << "Check duration: "
            << utils::duration(start, finish)
            << " ns" << std::endl;
}

void MnistNNTrainer::feed(mlp::Vector &v) {
  nn_->feed(std::move(v));
}

size_t MnistNNTrainer::max_feed() const {
  return freemath::max_feed(nn_->result_vector());
}

const mlp::Vector &MnistNNTrainer::result_vector() const {
  return nn_->result_vector();
}

int MnistNNTrainer::result() const {
  return freemath::max_feed(nn_->result_vector());
}


void Utils::vector2bmp(const std::vector<uint8_t> &v, BMP &m) {
  m.fill_region(0, 0, 28, 28, 0x00, 0xFF, 0x00, 0xFF);
  for (int x = 0; x < 28; x++) {
    for (int y = 0; y < 28; y++) {
      int px = 255 - v[x + 28 * y];
      m.set_pixel(x, 27 - y, px, px, px, 0xFF);
    }
  }
}

} // namespace mnist

std::ostream &operator<<(std::ostream &out, const mlp::Vector &v) {
  return out;
}
std::ostream &operator<<(std::ostream &out, const BMP &bmp) {
  for (int y = map_height - 1; y >= 0; --y) {
    for (int x = 0; x < map_width; ++x) {
      auto data = bmp.get_bgra(x, y);
      out << 0xFF - data[0];
      if (x != map_width - 1 || y != map_height - 1) {
        out << ",";
      }
    }
  }
  return out;
}

