#include <cassert>

#include "MLPNeuralNetwork.h"

namespace mlp {

MLPNeuralNetwork::MLPNeuralNetwork(MLPNNStructure &&structure)
    : structure_(structure),
      request_(structure.at(0)),
      expected_(structure.back()) {
  for (size_t i = 0; i < structure.size() - 1; ++i) {
    size_t curr = structure.at(i);
    size_t next = structure.at(i + 1);
    //layers_.push_back(std::move(MLPNNLayer{curr, next}));
    layers_.emplace_back(curr, next);
  }
}
void MLPNeuralNetwork::feed_forward() {
  layers_[0].feed_hidden(request_);
  for (int i = 1; i < layers_.size(); ++i)
    layers_[i].feed_hidden(layers_.at(i - 1).hid_vector());
}
void MLPNeuralNetwork::back_propagate() {
  //! Feed err_ vector for the last layer
  layers_.back().feed_err_out(expected_);
  //! Feed \b previous err_ vector layer using
  //! \b next layer data
  for (int i = (int) layers_.size() - 2; i >= 0; --i) {
    layers_[i].feed_err_hid(layers_.at(i + 1));
  }
  //! Update weight matrix wm_ from next to previous
  for (int i = (int) layers_.size() - 1; i > 0; --i) {
    layers_[i].update_wm(layers_.at(i - 1).hid_vector());
  }
  //! Update weights for the first layer with
  //! input  request_;
  layers_.front().update_wm(request_);
}
void MLPNeuralNetwork::new_lesson(Vector &&request, Vector &&result) {
  assert(request.width() == input_neurons_size());
  assert(result.width() == output_neurons_size());

  request_.swap(std::move(request));
  expected_.swap(std::move(result));
  feed_forward();
  back_propagate();
}
void MLPNeuralNetwork::feed(Vector &&request) {
  assert(request.width() == input_neurons_size());

  request_.swap(std::move(request));
  feed_forward();
}
size_t MLPNeuralNetwork::input_neurons_size() {
  return structure_.front();
}
size_t MLPNeuralNetwork::output_neurons_size() {
  return structure_.back();
}
Vector &MLPNeuralNetwork::result_vector() {
  return layers_.back().hid_vector();
}

} // namespace mlp
