#include <iostream>
#include <cstring>
#include "sstream"

#include "Mnist.h"
#include "BMP.h"
#include "Vector.h"

using namespace mnist;

int main(int argc, char **argv) {
  std::cout.imbue(std::locale(""));
  std::cout << "=========================" << std::endl;

  std::cout << "Enter number of lessons to learn (0..60'000)" << std::endl;
  constexpr const int bufsz = 512;
  constexpr const char *fname = "in/blanc.bmp";
  std::string ans;
  std::cin >> ans;
  int bound = std::stoi(ans);

  MnistNNTrainer mt{MnistNNCreator::spawn_nn()};
  mt.train_nn(bound);

  do {
    std::cout << "=========================" << std::endl;
    std::cout << "Edit "
              << fname << " of size 28x28 "
              << "and then press Enter.\n"
              << "q to quit" << std::endl;
    std::cin.clear();
    std::cin.ignore(1);
    char buf[bufsz];
    std::cin.get(buf, bufsz);
    BMP in(fname);
    std::stringstream ss;
    ss << in;
    std::string s;
    size_t symbol = 0;
    mlp::Vector request{784, mlp::Vector::TINY_DOUBLE};
    while (std::getline(ss, s, ',')) {
      double val = ((double) std::stoi(s) / 255 * 0.99) + 0.01;
      request[symbol] = val;
      ++symbol;
    }
    mt.feed(request);
    std::cout << "Entered digit: " << mt.max_feed() << std::endl;
  } while (ans != "q");

  std::cout << "Finish" << std::endl;
  return 1;
}

