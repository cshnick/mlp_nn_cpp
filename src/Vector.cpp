#include <iostream>

#include "Vector.h"

namespace mlp {
Vector::Vector(size_t width, InitOpt init_with)
    : width_(width),
      data(width ? new double[width] : nullptr, [width = width](double const *d) {
        delete[] d;
        d = nullptr;
      }) {
  for (int i = 0; i < width; ++i) {
    double init_value = 0.0;
    switch (init_with) {
      case TINY_DOUBLE: init_value = 0.01;
        break;
      default: break;
    }
    data[i] = init_value;
  }
}
double &Vector::operator[](size_t index) {
  return data[index];
}
const double &Vector::operator[](size_t index) const{
  return data[index];
}
size_t Vector::width() const {
  return width_;
}
void Vector::swap(Vector &&other) {
  this->width_ = other.width_;
  other.width_ = 0;
  this->data.swap(other.data);
}
void Vector::print() const {
  std::cout << "Vector of width " << width_ << std::endl;
  for (int i = 0; i < width_; ++i) {
    std::cout << data[i] << " ";
  }
  std::cout << "\n=======================" << std::endl;
}

} // namespace mlp

size_t freemath::max_feed(mlp::Vector &v) {
  int max_index = 0;
  for (int i = 0; i < v.width(); ++i) {
    if ((1.0 - v[i]) < (1 - v[max_index])) {
      max_index = i;
    }
  }
  return max_index;
}
