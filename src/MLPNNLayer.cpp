#include "MLPNNLayer.h"

namespace mlp {

MLPNNLayer::MLPNNLayer(size_t in, size_t out)
    : lsz_{in, out},
      wm_({in, out}, Matrix::RANDOM),
      hid_{out},
      err_{out} {
}

MLPNNLayer::MLPNNLayer(MLPNNLayer &&other) noexcept
    : lsz_(0, 0),
      wm_({0, 0}),
      hid_(0),
      err_(0) {
  this->lsz_ = other.lsz_;
  other.lsz_ = {0, 0};
  this->wm_.swap(std::move(other.wm_));
  this->hid_.swap(std::move(other.hid_));
  this->err_.swap(std::move(other.err_));
}

void MLPNNLayer::update_wm(Vector &data) {
  auto[in, out] = lsz_;
  for (int o = 0; o < out; o++) {
    for (int i = 0; i < in; i++) {
      wm_[i][o] += (lr * err_[o] * data[i]);
    }
    wm_[in][o] += (lr * err_[o]);
  }
}
void MLPNNLayer::feed_hidden(Vector &input) {
  auto[in, out] = lsz_;
  for (int hid = 0; hid < out; ++hid) {
    double acc_sig = 0.0;
    for (int inp = 0; inp < in; ++inp) {
      acc_sig += input[inp] * wm_[inp][hid];
    }
    acc_sig += wm_[in][hid];
    hid_[hid] = freemath::sigmoid(acc_sig);
  }
}
void MLPNNLayer::feed_err_out(Vector &target) {
  auto[in, out] = lsz_;
  for (int o = 0; o < out; ++o) {
    err_[o] = (target[o] - hid_[o]) * freemath::dsigmoid(hid_[o]);
  }
}
void MLPNNLayer::feed_err_hid(MLPNNLayer &next) {
  auto[next_in, next_out] = next.lsz_;
  for (int hid = 0; hid < next_in; hid++) {
    err_[hid] = 0.0;
    for (int o = 0; o < next_out; o++) {
      err_[hid] += next.err_[o] * next.wm_[hid][o];
    }
    err_[hid] *= freemath::dsigmoid(hid_[hid]);
  }
}
Vector &MLPNNLayer::hid_vector() {
  return hid_;
}

} // namespace mlp