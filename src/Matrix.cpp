#include <iostream>

#include "Matrix.h"

namespace mlp {
Matrix::Matrix(std::pair<size_t, size_t> sz, InitOpt initWith)
    : size_(sz),
      data(new double *[sz.first + 1], [sz = sz](double *d[]) {
        auto[in, out] = sz;
        for (int i = 0; i < in + 1; ++i) {
          delete[] d[i];
          d[i] = nullptr;
        }
        delete[] d;
      }) {
  for (int i = 0; i < sz.first + 1; ++i) {
    data[i] = new double[sz.second];
  }
  switch (initWith) {
    case RANDOM: init_random();
      break;
    case ZEROES: init_zeroes();
      break;
    default: break;
  }

}
double *&Matrix::operator[](size_t index) {
  return data[index];
}
Size Matrix::size() {
  return size_;
}
size_t Matrix::sig_count() const {
  return size_.first;
}
size_t Matrix::out_count() const {
  return size_.second;
}
void Matrix::swap(Matrix &&other) {
  this->size_ = other.size_;
  other.size_ = {0, 0};
  this->data.swap(other.data);
}
void Matrix::print() const {
  auto[in, out] = size_;
  std::cout << "Matrix of size {" << in << "," << out << "}" << std::endl;
  std::cout << "[";
  for (int i = 0; i < in + 1; ++i) {

    std::cout << (i == in ? "=========================\n" : "")
              << "[";
    for (int j = 0; j < out; ++j) {
      std::cout << (i == in ? "(" : "")
                << data[i][j]
                << (i == in ? ")" : "")
                << ", ";
    }
    std::cout << "]" << std::endl;
  }
  std::cout << "]" << std::endl;
}
void Matrix::init_random() {
  auto[in, out] = size_;
  for (int i = 0; i < in + 1; ++i) {
    for (int j = 0; j < out; ++j) {
      data[i][j] = rand_weight();
    }
  }
}
void Matrix::init_zeroes() {
  auto[in, out] = size_;
  for (int i = 0; i < in + 1; ++i) {
    for (int j = 0; j < out; ++j) {
      data[i][j] = 0.0;
    }
  }
}

} // namespace mlp
